//
//  CompanyListCell.h
//  Spore Task
//
//  Created by manimegalai on 20/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CompanyListCell : UITableViewCell

@property(strong,nonatomic) UIImageView *imgLogo;
@property(strong,nonatomic) UILabel *lblCompanyName;
@property(strong,nonatomic) UILabel *lblWebsite;
@property(strong,nonatomic) UILabel *lblDec;
@end

NS_ASSUME_NONNULL_END
