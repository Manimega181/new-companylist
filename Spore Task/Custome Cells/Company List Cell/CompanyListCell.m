//
//  CompanyListCell.m
//  Spore Task
//
//  Created by manimegalai on 20/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import "CompanyListCell.h"

@implementation CompanyListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self prepareViews];
    }
    return self;
}

-(void)prepareViews{
    
    self.imgLogo = [[UIImageView alloc]init];
    
    self.lblCompanyName = [[UILabel alloc]init];
    self.lblCompanyName.font = [UIFont boldSystemFontOfSize:13.5];
    self.lblCompanyName.textColor = [UIColor blackColor];
    
    self.lblWebsite = [[UILabel alloc]init];
    self.lblWebsite.font = [UIFont systemFontOfSize:12.5];
    self.lblWebsite.textColor = [UIColor blueColor];
    
    self.lblDec = [[UILabel alloc]init];
    self.lblDec.font = [UIFont systemFontOfSize:12.5];
    self.lblDec.textColor = [UIColor grayColor];
    self.lblDec.numberOfLines = 0;
    
    [self addSubview:self.imgLogo];
    [self addSubview:self.lblCompanyName];
    [self addSubview:self.lblWebsite];
    [self addSubview:self.lblDec];
    
    [self frameForViews];
}

-(void)frameForViews{
    CGFloat x = 0, y = 0, width = 0, height = 0;
    
    x   = 15;
    y   = 15;
    width   = 60;
    height  = 60;
    self.imgLogo.frame = CGRectMake(x, y, width, height);
    
    x   = (self.imgLogo.frame.origin.x) + (self.imgLogo.frame.size.width) + 15;
    y   = 15;
    width   = [UIScreen mainScreen].bounds.size.width - x - 15;
    height  = 25;
    self.lblCompanyName.frame = CGRectMake(x, y, width, height);
    
    x   = (self.lblCompanyName.frame.origin.x);
    y   = (self.lblCompanyName.frame.origin.y) + (self.lblCompanyName.frame.size.height);
    width   = [UIScreen mainScreen].bounds.size.width - x - 15;
    height  = 25;
    self.lblWebsite.frame = CGRectMake(x, y, width, height);
    
    x   = (self.lblCompanyName.frame.origin.x);
    y   = (self.lblWebsite.frame.origin.y) + (self.lblWebsite.frame.size.height);
    width   = [UIScreen mainScreen].bounds.size.width - x - 15;
    height  = 60;
    self.lblDec.frame = CGRectMake(x, y, width, height);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
