//
//  SortingObject.m
//  Spore Task
//
//  Created by Promoth on 21/3/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import "SortingObject.h"

@implementation SortingObject
@synthesize delegate;

-(id)initWithObject:(CompanyList *)SortObject SearchString:(NSString *)strSearchString{
    self = [super init];
    if (self) {
        [self fetchObject:SortObject SearchString:strSearchString];
    }
    return self;
}

-(void)fetchObject:(CompanyList *)SortObject SearchString:(NSString *)strSearchString{
    NSLog(@"SearchString - %@",strSearchString);
    self.listObject       = SortObject;
    self.strSearchString  = strSearchString;
    self.isFiltered       = NO;
    
    if (self.strSearchString.length != 0) {
        self.isFiltered = YES;
        [self searchRestaurant];
        self.isChosed = YES;
    }
        
    self.isChosed = NO;
    [delegate SortingObject:self];
}



-(void)reinitializeObject{
    self.aryCompname  = nil;
    self.aryCompWeb        = nil;
    self.aryComplogo      = nil;
    self.aryCompDesc      = nil;
    
    
    self.aryCompname   = [NSMutableArray new];
    self.aryCompWeb         = [NSMutableArray new];
    self.aryComplogo       = [NSMutableArray new];
    self.aryCompDesc       = [NSMutableArray new];
    
}

-(void)tempMemAllocation{
    self.aryIndex  = nil;
    self.aryIndex  = [NSMutableArray new];
    
    self.aryTempCompName  = nil;
    self.aryTempCompWeb       = nil;
    self.aryTempCompDesc      = nil;
    self.aryTempCompLogo     = nil;
    
  
    
    self.aryTempCompName  = [NSMutableArray new];
    self.aryTempCompWeb        = [NSMutableArray new];
    self.aryTempCompDesc      = [NSMutableArray new];
    self.aryTempCompLogo      = [NSMutableArray new];
    
 
    
    if (self.isChosed) {
        self.aryTempCompName  = [self.aryCompname mutableCopy];
        self.aryTempCompWeb        = [self.aryCompWeb mutableCopy];
        self.aryTempCompDesc      = [self.aryCompDesc mutableCopy];
        self.aryTempCompLogo      = [self.aryComplogo mutableCopy];
      
       
    }else{
        self.aryTempCompName  = [self.listObject.aryCompName mutableCopy];
        self.aryTempCompWeb        = [self.listObject.aryCompWebsite mutableCopy];
        self.aryTempCompDesc      = [self.listObject.aryCompDesc mutableCopy];
        self.aryTempCompLogo      = [self.listObject.aryCompLogo mutableCopy];
       
    }
    [self reinitializeObject];
}

#pragma mark - SEARCH BY RESTAURANT

-(void)searchRestaurant{
    [self tempMemAllocation];
    for (NSInteger i = 0;i < self.aryTempCompName.count;++i) {
        NSString *strName = self.aryTempCompName[i];
        NSRange titleResultsRange = [strName rangeOfString:self.strSearchString options:NSCaseInsensitiveSearch];
        
        if (titleResultsRange.length != 0) {
            [self.aryIndex addObject:[NSString stringWithFormat:@"%ld",(long)i]];
        }
    }
    [self initializingObject];
}


#pragma mark - INITIALIZING OBJECT

-(void)initializingObject{
    if (self.aryIndex.count != 0) {
        for (NSInteger i = 0;i < self.aryIndex.count;++i) {
            NSInteger indexPath = [self.aryIndex[i] integerValue];
            [self.aryCompname addObject:self.aryTempCompName[indexPath]];
            [self.aryComplogo addObject:self.aryTempCompLogo[indexPath]];
            [self.aryCompWeb addObject:self.aryTempCompWeb[indexPath]];
            [self.aryCompDesc addObject:self.aryTempCompDesc[indexPath]];
           
    
        }
    }
}
@end
