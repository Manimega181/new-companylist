//
//  SortingObject.h
//  Spore Task
//
//  Created by Promoth on 21/3/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompanyList.h"

@class SortingObject;

@protocol SortingObjectDelegate <NSObject>

-(void)SortingObject:(SortingObject *)sortingObject;

@end

NS_ASSUME_NONNULL_BEGIN

@interface SortingObject : NSObject

@property(strong,nonatomic) NSMutableArray *aryTempCompName;
@property(strong,nonatomic) NSMutableArray *aryTempCompLogo;
@property(strong,nonatomic) NSMutableArray *aryTempCompWeb;
@property(strong,nonatomic) NSMutableArray *aryTempCompDesc;


@property(nonatomic)BOOL isChosed;
@property(nonatomic)BOOL isFiltered;
@property(strong,nonatomic) NSString *strSearchString;
@property(strong,nonatomic) NSString *strCuisine;
@property(strong,nonatomic) NSString *strDistance;
@property(strong,nonatomic) NSMutableArray *aryIndex;
@property(strong,nonatomic) NSMutableArray *aryRestStatus;
@property(strong,nonatomic) NSMutableArray *aryRating;
//@property(strong,nonatomic) NSMutableArray *aryRatingPoint;
@property(strong,nonatomic) NSMutableArray *aryCompname;
@property(strong,nonatomic) NSMutableArray *aryCompWeb;
@property(strong,nonatomic) NSMutableArray *aryComplogo;
@property(strong,nonatomic) NSMutableArray *aryCompDesc;
@property(strong,nonatomic) CompanyList *listObject;

@property(weak,nonatomic) id<SortingObjectDelegate>delegate;

-(id)initWithObject:(CompanyList *)SortObject SearchString:(NSString *)strSearchString;
-(void)fetchObject:(CompanyList *)SortObject SearchString:(NSString *)strSearchString;


@end

NS_ASSUME_NONNULL_END
