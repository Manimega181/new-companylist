//
//  CompanyList.h
//  Spore Task
//
//  Created by Promoth on 21/3/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CompanyList;
@protocol CompanyListDelegate <NSObject>

-(void)CompanyList:(CompanyList *)listObject;

@end

NS_ASSUME_NONNULL_BEGIN

@interface CompanyList : NSObject{
    
}
@property(nonatomic)BOOL isValid;

@property(strong,nonatomic) NSString *strMessage;

@property(strong,nonatomic) NSMutableArray *aryCompName;
@property(strong,nonatomic) NSMutableArray *aryCompLogo;
@property(strong,nonatomic) NSMutableArray *aryCompWebsite;
@property(strong,nonatomic) NSMutableArray *aryCompDesc;

@property(strong,nonatomic) NSMutableArray *aryMemName;
@property(strong,nonatomic) NSMutableArray *aryMemAge;
@property(strong,nonatomic) NSMutableArray *aryMemPhone;
@property(strong,nonatomic) NSMutableArray *aryMemEmail;

@property(weak,nonatomic) id<CompanyListDelegate>delegate;

-(id)initWithUrl:(NSString *)strURL;
-(void)fetchUrl:(NSString *)strURL;

@end

NS_ASSUME_NONNULL_END
