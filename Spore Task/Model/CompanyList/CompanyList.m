//
//  CompanyList.m
//  Spore Task
//
//  Created by Promoth on 21/3/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import "CompanyList.h"
#import "ApiClient.h"

@implementation CompanyList

@synthesize delegate;

-(id)initWithUrl:(NSString *)strURL{
    self = [super init];
    if (self) {
        [self fetchUrl:strURL];
    }
    return self;
}

-(void)fetchUrl:(NSString *)strURL{
    ApiClient *client = [[ApiClient alloc]init];
    [client authenticateWithUrl:strURL withCompletion:^(id responseObject, NSError *error) {
        dispatch_async(dispatch_get_main_queue(),^{
            NSLog(@"LIST COMPANY SCREEN - %@",responseObject);
            
            self.aryCompName       = nil;
            self.aryCompLogo = nil;
            self.aryCompWebsite  = nil;
            self.aryCompDesc  = nil;
            
            self.aryMemName = nil;
            self.aryMemAge   = nil;
            self.aryMemPhone = nil;
            self.aryMemEmail = nil;
            
            if (!responseObject) {
                self.isValid = NO;
                self.strMessage = @"Oops something went wrong...";
            }else{
                
                self.isValid = YES;
                
                self.aryCompName = [responseObject valueForKeyPath:@"company"];
                self.aryCompLogo = [responseObject valueForKeyPath:@"logo"];
                self.aryCompWebsite = [responseObject valueForKeyPath:@"website"];
                self.aryCompDesc = [responseObject valueForKeyPath:@"about"];
                
               /* NSNumber *isSuccess =(NSNumber *)[responseObject valueForKeyPath:@"result.success"];
                if([isSuccess boolValue] == YES){
                    self.isValid = YES;
                    
                    self.aryCompName = [responseObject valueForKeyPath:@"company"];
                    self.aryCompLogo = [responseObject valueForKeyPath:@"logo"];
                    self.aryCompWebsite = [responseObject valueForKeyPath:@"website"];
                    self.aryCompDesc = [responseObject valueForKeyPath:@"about"];
                    
            
                }else{
                    self.isValid = NO;
                }*/
            }
            [self->delegate CompanyList:self];
        });
    }];
}

@end
