//
//  ApiClient.h
//  Tasty Dragon
//
//  Created by manimegalai on 20/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface ApiClient : NSObject

//POST METHOD

-(void)authenticateUserWithCredentials:(NSDictionary *)credentials URL:(NSString *)strURL withCompletion:(void(^)(id responseObject, NSError *error))completion;

//GET METHOD

-(void)authenticateWithUrl:(NSString *)strUrl withCompletion:(void(^)(id responseObject, NSError *error))completion;

@end
