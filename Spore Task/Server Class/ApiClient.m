//
//  ApiClient.m
//  Tasty Dragon
//
//  Created by manimegalai on 20/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//


#import "NetworkRechabilityManager.h"
#import "ApiClient.h"

@implementation ApiClient

// POST METHOD

-(void)authenticateUserWithCredentials:(NSDictionary *)credentials URL:(NSString *)strURL withCompletion:(void (^)(id, NSError *))completion{
    if ([[[NetworkRechabilityManager sharedManager]reachability] isReachable]) {
        NSString *URL;
       
        URL = [NSString stringWithFormat:@"%@",strURL];
        
        NSLog(@"URL = %@",URL);
        NSURL *requestURL  = [NSURL URLWithString:URL];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer.timeoutInterval = 150;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",@"text/plain",nil];
        [manager POST:requestURL.absoluteString parameters:credentials progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            completion(responseObject,nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSDictionary *ResultResponse = @{
                                                 @"success"  : @(0),
                                                 @"message"  : @"Oops something went wrong"
                                             };
            NSDictionary *responseDictionary = @{
                                                 @"result" : ResultResponse
                                                 };
            completion(responseDictionary,nil);
        }];
    }else{
        NSDictionary *ResultResponse = @{
                                         @"success"  : @(0),
                                         @"message"  : @"No Internet Connection"
                                         };
        
        NSDictionary *responseDictionary = @{
                                             @"result" : ResultResponse
                                             };
        completion(responseDictionary,nil);
    }
}

// GET METHOD

-(void)authenticateWithUrl:(NSString *)strUrl withCompletion:(void (^)(id, NSError *))completion{
    if ([[[NetworkRechabilityManager sharedManager]reachability] isReachable]) {
        NSURL *URL = [NSURL URLWithString:strUrl];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer.timeoutInterval = 300;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            completion(responseObject,nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSLog(@"error %@",error);
        }];
    }else{
        NSDictionary *ResultResponse = @{
                                         @"success"  : @(0),
                                         @"message"  : @"No Internet Connection"
                                         };
        
        NSDictionary *responseDictionary = @{
                                             @"result" : ResultResponse
                                             };
        completion(responseDictionary,nil);
    }
}

@end
