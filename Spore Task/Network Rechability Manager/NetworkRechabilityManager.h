//
//  NetworkRechabilityManager.h
//  Food Fleet
//
//  Created by manimegalai on 20/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface NetworkRechabilityManager : NSObject
@property (strong, nonatomic) Reachability *reachability;
+ (NetworkRechabilityManager *)sharedManager;
+ (BOOL)isReachable;
+ (BOOL)isUnreachable;
+ (BOOL)isReachableViaWWAN;
+ (BOOL)isReachableViaWiFi;
@end
