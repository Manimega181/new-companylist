//
//  NetworkRechabilityManager.m
//  Food Fleet
//
//  Created by manimegalai on 20/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import "NetworkRechabilityManager.h"

@implementation NetworkRechabilityManager

+ (NetworkRechabilityManager *)sharedManager {
    static NetworkRechabilityManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    return _sharedManager;
}

- (id)init {
    self = [super init];
    if (self) {
        self.reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
        [self.reachability startNotifier];
    }
    return self;
}

#pragma mark - #pragma mark Memory Management

- (void)dealloc {
    if (_reachability) {
        [_reachability stopNotifier];
    }
}

#pragma mark - #pragma mark Class Methods

+ (BOOL)isReachable {
    return [[[NetworkRechabilityManager sharedManager] reachability] isReachable];
}

+ (BOOL)isUnreachable {
    return ![[[NetworkRechabilityManager sharedManager] reachability] isReachable];
}

+ (BOOL)isReachableViaWWAN {
    return [[[NetworkRechabilityManager sharedManager] reachability] isReachableViaWWAN];
}

+ (BOOL)isReachableViaWiFi {
    return [[[NetworkRechabilityManager sharedManager] reachability] isReachableViaWiFi];
}

@end
