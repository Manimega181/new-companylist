//
//  AppDelegate.h
//  Spore Task
//
//  Created by manimegalai on 19/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

