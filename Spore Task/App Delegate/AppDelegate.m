//
//  AppDelegate.m
//  Spore Task
//
//  Created by manimegalai on 19/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import "AppDelegate.h"
#import "CompanyListController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [[UINavigationController alloc]initWithRootViewController:[CompanyListController new]];
    [self.window makeKeyAndVisible];
    return YES;
}


@end
