//
//  main.m
//  Spore Task
//
//  Created by manimegalai on 19/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
