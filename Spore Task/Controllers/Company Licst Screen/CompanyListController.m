//
//  CompanyListController.m
//  Spore Task
//
//  Created by manimegalai on 20/03/19.
//  Copyright © 2019 Spore Task. All rights reserved.
//

#import "CompanyListController.h"
#import "NetworkRechabilityManager.h"
#import "CompanyListCell.h"
#import "MBProgressHUD.h"
#import "CompanyList.h"
#import "SortingObject.h"
#import "ApiClient.h"

@interface CompanyListController ()
<
 UITableViewDelegate,
 UITableViewDataSource,
 UISearchBarDelegate,
CompanyListDelegate,
SortingObjectDelegate
>
{
    CompanyList *compList;
    SortingObject   *sortObject;
    MBProgressHUD    *hud;
    
    /*----------- NAVIGATION BAR VIEW ----------------*/
    
    UIView    *viewNavbar;
    UILabel   *lblTitle;
    UIButton  *btnSearch;
    UIButton  *btnSort;
    
    UISearchBar *searchList;
    
    UITableView *tblCompayView;
    
    /*-------- Filter By Parameter ----------*/
    
    UIView   *viewParameter;
    UIView   *viewParamHead;
    UIButton *btnFltrback;
    UILabel  *lblFltrBack;
    UILabel  *lblFltrTitle;;
    UIView   *container;
    UILabel  *lblFilterBy;
    UIButton *btnAscending;
    UIButton *btnDescending;
    UIButton *btnReset;
    UIButton *btnSearchIcon;
    UIButton *btnApplyFilter;
    
    UIScrollView *scrollContiner;
    UITableView  *tblCuisine;
    
    NSInteger tableheight;
    
    NSString *strSearchString;
    NSMutableArray *aryIndex;
    
    NSInteger indexValue;
    
    BOOL isFiltered;
    
}

@end

@implementation CompanyListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    tableheight = 0;
    isFiltered       = NO;
    strSearchString  = @"";
    [self prepareHUD];
    [self prepareNavBar];
    [self prepareData];
    [self prepareFilter];
    [self prepareView];
}

#pragma mark - PREPARE HUD

-(void)prepareHUD{
    hud = [[MBProgressHUD alloc]initWithView:self.view];
    hud.layer.zPosition        = 1.0f;
    hud.bezelView.color        = [UIColor blackColor];
    hud.mode                   = MBProgressHUDModeIndeterminate;
    hud.animationType          = MBProgressHUDAnimationZoomIn;
    hud.contentColor    = [UIColor whiteColor];
}

#pragma mark - PREPARE FILTER

-(void)prepareFilter{
    scrollContiner = [[UIScrollView alloc]init];
    scrollContiner.showsVerticalScrollIndicator = NO;
    scrollContiner.showsHorizontalScrollIndicator = NO;

    
    viewParameter = [[UIView alloc] init];
    viewParameter.backgroundColor = [UIColor clearColor];
    viewParameter.layer.zPosition = 1.0f;
    viewParameter.layer.borderColor  = [UIColor clearColor].CGColor;
    
    viewParamHead = [[UIView alloc] init];
    viewParamHead.backgroundColor = [UIColor redColor];
    viewParamHead.userInteractionEnabled = YES;
    
    btnFltrback = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnFltrback setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [btnFltrback addTarget:self action:@selector(filterBackEvent) forControlEvents:UIControlEventTouchUpInside];
    
    lblFltrBack                 = [[UILabel alloc]init];
    lblFltrBack.text            = @"Back";
    lblFltrBack.textColor       = [UIColor whiteColor];
    lblFltrBack.textAlignment   = NSTextAlignmentLeft;
    lblFltrBack.font            = [UIFont systemFontOfSize:12];
    lblFltrBack.userInteractionEnabled = YES;
    [lblFltrBack addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(filterBackEvent)]];
    
    lblFltrTitle                 = [[UILabel alloc]init];
    lblFltrTitle.text            = @"Sort Company";
    lblFltrTitle.textColor       = [UIColor whiteColor];
    lblFltrTitle.textAlignment   = NSTextAlignmentCenter;
    lblFltrTitle.font            = [UIFont systemFontOfSize:15];
    lblFltrTitle.numberOfLines   = 0;
    
    container = [[UIView alloc] init];
    container.backgroundColor = [UIColor whiteColor];
    
    btnReset = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnReset addTarget:self action:@selector(clearFilter) forControlEvents:UIControlEventTouchUpInside];
    btnReset.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [btnReset setTitle:@"Reset" forState:UIControlStateNormal];
    [btnReset setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnReset.layer.cornerRadius = 3.0f;
    btnReset.clipsToBounds = YES;
    
    btnAscending  = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnAscending addTarget:self action:@selector(FilterTypeAll) forControlEvents:UIControlEventTouchUpInside];
    btnAscending.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [btnAscending setTitle:@"Ascending" forState:UIControlStateNormal];
    [btnAscending setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    btnAscending.layer.cornerRadius = 4.0f;
    btnAscending.layer.backgroundColor  = [UIColor whiteColor].CGColor;
    btnAscending.layer.borderColor  = [UIColor redColor].CGColor;
    btnAscending.layer.borderWidth  = 1.0f;
    
    btnDescending = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnDescending addTarget:self action:@selector(FilterTypeColl) forControlEvents:UIControlEventTouchUpInside];
    btnDescending.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [btnDescending setTitle:@"Descending" forState:UIControlStateNormal];
    [btnDescending setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    btnDescending.layer.cornerRadius = 4.0f;
    btnDescending.layer.backgroundColor  = [UIColor whiteColor].CGColor;
    btnDescending.layer.borderColor = [UIColor redColor].CGColor;
    btnDescending.layer.borderWidth  = 1.0f;
    
    btnApplyFilter = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnApplyFilter addTarget:self action:@selector(applyFilter) forControlEvents:UIControlEventTouchUpInside];
    btnApplyFilter.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [btnApplyFilter setTitle:@"APPLY " forState:UIControlStateNormal];
    [btnApplyFilter setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnApplyFilter.backgroundColor = [UIColor redColor];
    btnApplyFilter.layer.cornerRadius = 4.0f;
    btnApplyFilter.layer.backgroundColor  = [UIColor redColor].CGColor;
    btnApplyFilter.layer.borderColor = [UIColor redColor].CGColor;
    btnApplyFilter.layer.borderWidth  = 1.0f;
    
    lblFilterBy                 = [[UILabel alloc]init];
    lblFilterBy.text            = @"Sort By";
    lblFilterBy.textColor       = [UIColor redColor];
    lblFilterBy.textAlignment   = NSTextAlignmentLeft;
    lblFilterBy.font            = [UIFont systemFontOfSize:15];
 
    tblCuisine = [[UITableView alloc]init];
    tblCuisine.allowsMultipleSelection = NO;
    tblCuisine.backgroundColor = [UIColor whiteColor];
    tblCuisine.delegate = self;
    tblCuisine.dataSource = self;
    
    [self.view addSubview:viewParameter];
    [viewParameter addSubview:viewParamHead];
    [viewParameter addSubview:scrollContiner];
    [scrollContiner addSubview:container];
    [container addSubview:lblFilterBy];
    [container addSubview:btnAscending];
    [container addSubview:btnDescending];
    [container addSubview:tblCuisine];
    [container addSubview:btnApplyFilter];
    
    [viewParamHead addSubview:btnReset];
    [viewParamHead addSubview:btnFltrback];
    [viewParamHead addSubview:lblFltrBack];
    [viewParamHead addSubview:lblFltrTitle];
    
    [self.view addSubview:hud];
    [self frameForFilter];
}
-(void)frameForFilter{
    CGFloat x = 0,y = 0,width = 0,height = 0 ,scrollContent = 0;
    
    x  =  0;
    y  =  64 + 3;
    width  = [UIScreen mainScreen].bounds.size.width;
    height = [UIScreen mainScreen].bounds.size.height - 104 - 49;
    scrollContiner.frame = CGRectMake(x, y, width, height);
    
    x  =  0;
    y  =  -[UIScreen mainScreen].bounds.size.height;
    width  = [UIScreen mainScreen].bounds.size.width ;
    height = [UIScreen mainScreen].bounds.size.height - 49 ;
    viewParameter.frame = CGRectMake(x, y, width, height);
    
    x  =  0;
    y  =  5;
    width  = [UIScreen mainScreen].bounds.size.width;
    height = 160;
    container.frame = CGRectMake(x, y, width, height);
    
    x  =  0;
    y  =  0;
    width  = [UIScreen mainScreen].bounds.size.width ;
    height = 64;
    viewParamHead.frame = CGRectMake(x, y, width, height);
    
    x  = 10;
    y  = (64/2) - 3;
    width = 25;
    height = 25;
    btnFltrback.frame = CGRectMake(x, y, width, height);
    
    x = (btnFltrback.frame.origin.x + btnFltrback.frame.size.width) + 5;
    y  = (64/2) - 6;
    width = 70;
    height = 30;
    lblFltrBack.frame = CGRectMake(x, y, width, height);
    
    x        =  [UIScreen mainScreen].bounds.size.width/2 - 180/2;
    y        =  (64/2) - 6;
    width    =  180;
    height   =  30;
    lblFltrTitle.frame  =  CGRectMake(x, y, width, height);
    
    x  =  [UIScreen mainScreen].bounds.size.width - 95;
    y  =  (64/2) - 6;
    width  = 100;
    height = 30;
    btnReset.frame = CGRectMake(x, y, width, height);
    
    x  =  15;
    y  =  10;
    width  = 100;
    height = 35;
    lblFilterBy.frame = CGRectMake(x, y, width, height);
    
    x  = 40;
    y = (lblFilterBy.frame.origin.y + lblFilterBy.frame.size.height) + 5;
   // y  = GET_Y_MAX(lblFilterBy) + 5;
    width = 100;
    height= 35;
    btnAscending.frame = CGRectMake(x, y, width, height);
    
 //   x  = (btnAscending.frame.origin.x + btnAscending.frame.size.width) + 5;
    x = [UIScreen mainScreen].bounds.size.width - btnDescending.frame.size.width - 40;
    y = (lblFilterBy.frame.origin.y + lblFilterBy.frame.size.height) + 5;
    width = 100;
    height = 35;
    btnDescending.frame = CGRectMake(x, y, width, height);
    

    x  =  5;
    y  =  btnDescending.frame.origin.y + btnDescending.frame.size.height + 20;
    width  = [UIScreen mainScreen].bounds.size.width - 10;
    height = 40;
    btnApplyFilter.frame = CGRectMake(x, y, width, height);;
    
    y += height + 30 * 10  - 64;
    scrollContent = 200;
    scrollContiner.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width,scrollContent);
}

#pragma mark - PREPARE NAV BAR

-(void)prepareNavBar{
    
    viewNavbar = [[UIView alloc]init];
    viewNavbar.backgroundColor = [UIColor redColor];
    viewNavbar.userInteractionEnabled = YES;
    
    btnSearch = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnSearch setBackgroundImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [btnSearch addTarget:self action:@selector(searchEvent) forControlEvents:UIControlEventTouchUpInside];
    
    btnSort = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnSort setBackgroundImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
    [btnSort addTarget:self action:@selector(filterEvent) forControlEvents:UIControlEventTouchUpInside];
    
    lblTitle                 = [[UILabel alloc]init];
    lblTitle.text            = @"Company List";
    lblTitle.textColor       = [UIColor whiteColor];
    lblTitle.textAlignment   = NSTextAlignmentCenter;
    lblTitle.font            = [UIFont systemFontOfSize:16];
    lblTitle.numberOfLines   = 0;
    
    searchList = [[UISearchBar alloc]init];
    searchList.placeholder    = @"Search Company";
    [searchList setBackgroundImage:[[UIImage alloc]init]];
    searchList.delegate    = self;
    searchList.hidden      = YES;
    searchList.userInteractionEnabled = YES;
    
    UITextField *searchField = [searchList valueForKey:@"searchField"];
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil]
     setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]
     
     forState:UIControlStateNormal];
    searchField.backgroundColor = [UIColor whiteColor];
    searchField.textColor = [UIColor darkGrayColor];
    
    
    [self.view addSubview:viewNavbar];
    [viewNavbar addSubview:btnSearch];
    [viewNavbar addSubview:btnSort];
    [viewNavbar addSubview:searchList];
    [viewNavbar addSubview:lblTitle];
    [self frameForNavbar];
}

-(void)frameForNavbar{
    CGFloat x = 0,y = 0,width = 0,height = 0;
    
    x        =  0;
    y        =  0;
    width    =  [UIScreen mainScreen].bounds.size.width;
    height   =  64;
    viewNavbar.frame  =  CGRectMake(x, y, width, height);;
    
    
    x        =  20;
    y        =  (64/2) - 6;
    width    =  [UIScreen mainScreen].bounds.size.width - x * 2;
    height   =  30;
    lblTitle.frame  =  CGRectMake(x, y, width, height);
    
    x        =  10 ;
    y        =  21;
    width    =  [UIScreen mainScreen].bounds.size.width - x - 40;
    height   =  40;
    searchList.frame  =  CGRectMake(x, y, width, height);
    
    x        =  [UIScreen mainScreen].bounds.size.width - 35;
    y        =  (64/2);
    width    =  22;
    height   =  22;
    btnSort.frame  =  CGRectMake(x, y, width, height);
    
    x        = btnSort.frame.origin.x - 38 ;
    y        = (64/2);
    width    = 22;
    height   = 22;
    btnSearch.frame = CGRectMake(x, y, width, height);
    
}

#pragma mark - Prepare Data

/*-(void)prepareData{
    
     UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = self.view.center;
    spinner.backgroundColor = [[UIColor alloc]initWithRed:241 green:231 blue:231 alpha:0.5];
    
    [self.view addSubview:spinner];
    
    if ([[[NetworkRechabilityManager sharedManager]reachability]isReachable]){
        ApiClient *client = [[ApiClient alloc]init];
         [spinner startAnimating];
        [client authenticateWithUrl:@"https://next.json-generator.com/api/json/get/Vk-LhK44U" withCompletion:^(id responseObject, NSError *error) {
            dispatch_async(dispatch_get_main_queue(),^{
        
                self->aryRestName = [NSMutableArray new];
                self->aryRestLogo = [NSMutableArray new];
                self->aryRestDect  = [NSMutableArray new];
                self->aryRestWebSite  = [NSMutableArray new];
                
                if (!responseObject) {
                    [spinner stopAnimating];
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                                   message:@"Error occured contact "
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {}];
                    
                    [alert addAction:defaultAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }else{
                    [spinner stopAnimating];
                    self->aryRestName = [responseObject valueForKeyPath:@"company"];
                    self->aryRestLogo = [responseObject valueForKeyPath:@"logo"];
                    self->aryRestDect  = [responseObject valueForKeyPath:@"about"];
                    self->aryRestWebSite  = [responseObject valueForKeyPath:@"website"];
                    
                    [self->tblCompayView reloadData];
                }
                
            });
        }];
    }else{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:@"Network unrechable"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}*/

#pragma mark - PREPARE DATA

-(void)prepareData{
    
    NSLog(@"Prepare data search string:%@",strSearchString);
    
    sortObject = [[SortingObject alloc]initWithObject:compList SearchString:strSearchString];
    sortObject.delegate = self;
    
    if (!compList) {
        compList = [[CompanyList alloc]initWithUrl:@"https://next.json-generator.com/api/json/get/Vk-LhK44U"];
        compList.delegate = self;
    }else{
        [compList fetchUrl:@"https://next.json-generator.com/api/json/get/Vk-LhK44U"];
    }
    
//    sortObject = [[SortingObject alloc] initWithObject:compList SearchString:@"https://next.json-generator.com/api/json/get/Vk-LhK44U"];
//
//    sortObject.delegate = self;
    
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
}

#pragma mark - LIST RESTAURANT DELEGATE

-(void)CompanyList:(CompanyList *)listObject{
    
    [hud hideAnimated:YES];
    if (listObject.isValid == YES) {
        [self prepareView];
        [self Tableheight];
        [self frameForView];
    }else{
        if (listObject.strMessage.length != 0){
          //  ALERT(nil,listRestaurant.strMessage);
        }
    }
    [tblCompayView reloadData];
    [self frameForView];
    [self frameForFilter];
}


#pragma mark - Prepare View

-(void)prepareView{
    
   /* searchList = [[UISearchBar alloc]init];
    searchList.placeholder    = @"Search Company";
    [searchList setBackgroundImage:[[UIImage alloc]init]];
    searchList.delegate    = self;
    searchList.userInteractionEnabled = YES;
    searchList.layer.borderColor = [UIColor grayColor].CGColor;
    searchList.layer.borderWidth = 1;
    searchList.layer.cornerRadius = 3.0f;
    
    UITextField *searchField = [searchList valueForKey:@"searchField"];
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil]
     setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]
     
     forState:UIControlStateNormal];
    searchField.backgroundColor = [UIColor whiteColor];
    
    searchField.textColor = [UIColor darkGrayColor];
    searchField.font = [UIFont systemFontOfSize:13];*/

    

    

    
    tblCompayView = [[UITableView alloc]init];
    tblCompayView.showsVerticalScrollIndicator = false;
    tblCompayView.showsHorizontalScrollIndicator = false;
    tblCompayView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tblCompayView.separatorColor = [UIColor lightGrayColor];
    tblCompayView.tableFooterView = [UIView new];
    tblCompayView.delegate = self;
    tblCompayView.dataSource = self;
    
  //  [self.view addSubview:searchList];
    [self.view addSubview:tblCompayView];
    [self frameForView];
}

-(void)frameForView{
    CGFloat x = 0,y = 0,width = 0,height = 0;
    
//    x = 10;
//    y = UIApplication.sharedApplication.statusBarFrame.size.height;
//    width = self.view.frame.size.width - x * 2;
//    height = 40;
//    searchList.frame = CGRectMake(x, y, width, height);
    
    
    x   = 0;
   // y = (searchList.frame.origin.y) + (searchList.frame.size.height);
    y = viewNavbar.frame.origin.y + viewNavbar.frame.size.height;
    width   = self.view.frame.size.width;
    height = tableheight + 150;
   // height  = self.view.frame.size.height - y;
    tblCompayView.frame = CGRectMake(x, y, width, height);
    
}


#pragma mark - UITableView Delegate && Data Source




#pragma mark - TABLE VIEW DELEGATE

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
        return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isFiltered) {
        return sortObject.aryCompname.count;
    }else{
        return compList.aryCompName.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        static NSString *cellIdentifier = @"Cell Reusable";
        CompanyListCell *cell   = (CompanyListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if(cell == nil){
            cell = [[CompanyListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.backgroundColor = [UIColor whiteColor];
        }
        
        if (isFiltered) {
            
            cell.lblCompanyName.text = [NSString stringWithFormat:@"%@",sortObject.aryCompname[indexPath.row]];
            cell.lblWebsite.text = [NSString stringWithFormat:@"%@",sortObject.aryCompWeb[indexPath.row]];
            cell.lblDec.text = [NSString stringWithFormat:@"%@",sortObject.aryCompDesc[indexPath.row]];
            
            NSString *url_Img_FULL = [NSString stringWithFormat:@"%@",sortObject.aryComplogo[indexPath.row]];
            cell.imgLogo.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img_FULL]]];
            
            
            CGSize maximumSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 90, 9999);
            NSString *myString = [NSString stringWithFormat:@"%@",sortObject.aryCompDesc[indexPath.row]];
            CGSize myStringSize = [myString sizeWithFont:[UIFont systemFontOfSize:12.5]
                                       constrainedToSize:maximumSize
                                           lineBreakMode:cell.lblDec.lineBreakMode];
            
            CGFloat x = 0, y = 0, width = 0, height = 0;
            
            x   = (cell.lblCompanyName.frame.origin.x);
            y   = (cell.lblWebsite.frame.origin.y) + (cell.lblWebsite.frame.size.height);
            width   = [UIScreen mainScreen].bounds.size.width - x - 15;
            height  = myStringSize.height + 15;
            cell.lblDec.frame = CGRectMake(x, y, width, height);
           
        }else{
            
            cell.lblCompanyName.text = [NSString stringWithFormat:@"%@",compList.aryCompName[indexPath.row]];
            cell.lblWebsite.text = [NSString stringWithFormat:@"%@",compList.aryCompWebsite[indexPath.row]];
            cell.lblDec.text = [NSString stringWithFormat:@"%@",compList.aryCompDesc[indexPath.row]];
            
            NSString *url_Img_FULL = [NSString stringWithFormat:@"%@",compList.aryCompLogo[indexPath.row]];
            cell.imgLogo.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img_FULL]]];
            
            
            CGSize maximumSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 90, 9999);
            NSString *myString = [NSString stringWithFormat:@"%@",compList.aryCompDesc[indexPath.row]];
            CGSize myStringSize = [myString sizeWithFont:[UIFont systemFontOfSize:12.5]
                                       constrainedToSize:maximumSize
                                           lineBreakMode:cell.lblDec.lineBreakMode];
            
            CGFloat x = 0, y = 0, width = 0, height = 0;
            
            x   = (cell.lblCompanyName.frame.origin.x);
            y   = (cell.lblWebsite.frame.origin.y) + (cell.lblWebsite.frame.size.height);
            width   = [UIScreen mainScreen].bounds.size.width - x - 15;
            height  = myStringSize.height + 15;
            cell.lblDec.frame = CGRectMake(x, y, width, height);
        }
        return cell;
}
/*-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        return 120;
}*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isFiltered == YES){
        
        UILabel *lblDec = [[UILabel alloc]init];
        lblDec.font = [UIFont systemFontOfSize:12.5];
        lblDec.textColor = [UIColor grayColor];
        lblDec.numberOfLines = 0;
        
        CGSize maximumSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 90, 9999);
        NSString *myString = [NSString stringWithFormat:@"%@",sortObject.aryCompDesc[indexPath.row]];
        CGSize myStringSize = [myString sizeWithFont:[UIFont systemFontOfSize:12.5]
                                   constrainedToSize:maximumSize
                                       lineBreakMode:lblDec.lineBreakMode];
        
        return 70 + myStringSize.height + 20;
        
    }else{
        UILabel *lblDec = [[UILabel alloc]init];
        lblDec.font = [UIFont systemFontOfSize:12.5];
        lblDec.textColor = [UIColor grayColor];
        lblDec.numberOfLines = 0;
        
        CGSize maximumSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 90, 9999);
        NSString *myString = [NSString stringWithFormat:@"%@",compList.aryCompDesc[indexPath.row]];
        CGSize myStringSize = [myString sizeWithFont:[UIFont systemFontOfSize:12.5]
                                   constrainedToSize:maximumSize
                                       lineBreakMode:lblDec.lineBreakMode];
        
        return 70 + myStringSize.height + 20;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  /*  if(isFiltered){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [UIView animateWithDuration: 0.5f
                         animations:^{
                             RestaurantDetailController *cntrlRest = [RestaurantDetailController new];
                             cntrlRest.strRestaurtantId = sortObject.aryRestid[indexPath.row];
                             //  NSLog(@"SORTING REST ID -%@",sortObject.aryRestid[indexPath.row]);
                             cntrlRest.strCuisine       = sortObject.aryCusNameLimit[indexPath.row];
                             //  NSLog(@"SORTING REST ID -%@",sortObject.aryCusNameLimit[indexPath.row]);
                             cntrlRest.strRestAdd       = self.strAreaname;
                             NAVIGATE_TO(cntrlRest, NO);
                         }];
    } else{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [UIView animateWithDuration: 0.5f
                         animations:^{
                             RestaurantDetailController *cntrlRest = [RestaurantDetailController new];
                             cntrlRest.strRestaurtantId = listRestaurant.aryRestid[indexPath.row];
                             //   NSLog(@"REST ID -%@",listRestaurant.aryRestid[indexPath.row]);
                             cntrlRest.strCuisine       = listRestaurant.aryCusNameLimit[indexPath.row];
                             //  NSLog(@"REST ID -%@",listRestaurant.aryCusNameLimit[indexPath.row]);
                             cntrlRest.strRestAdd       = self.strAreaname;
                             NAVIGATE_TO(cntrlRest, NO);
                         }];
    }*/
}







/*-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (isFiltered == YES) {
        if (sortObject.aryCompname.count != 0){
            return sortObject.aryCompname.count;
        }else{
            return 0;
        }
    }else{
        if (compList.aryCompName != nil){
            NSLog(@"REST NAME = %@",compList.aryCompName);
            if (compList.aryCompName.count != 0){
                return compList.aryCompName.count;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CompanyListCell *cell   = (CompanyListCell *)[tableView dequeueReusableCellWithIdentifier:@"Complay List Cell"];
    if(cell == nil){
        cell = [[CompanyListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Complay List Cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (isFiltered == YES){
        
        cell.lblCompanyName.text = [NSString stringWithFormat:@"%@",sortObject.aryCompname[indexPath.row]];
        cell.lblWebsite.text = [NSString stringWithFormat:@"%@",sortObject.aryCompWeb[indexPath.row]];
        cell.lblDec.text = [NSString stringWithFormat:@"%@",sortObject.aryCompDesc[indexPath.row]];
        
        NSString *url_Img_FULL = [NSString stringWithFormat:@"%@",sortObject.aryComplogo[indexPath.row]];
        cell.imgLogo.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img_FULL]]];
        
        
        CGSize maximumSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 90, 9999);
        NSString *myString = [NSString stringWithFormat:@"%@",sortObject.aryCompDesc[indexPath.row]];
        CGSize myStringSize = [myString sizeWithFont:[UIFont systemFontOfSize:12.5]
                                   constrainedToSize:maximumSize
                                       lineBreakMode:cell.lblDec.lineBreakMode];
        
        CGFloat x = 0, y = 0, width = 0, height = 0;
        
        x   = (cell.lblCompanyName.frame.origin.x);
        y   = (cell.lblWebsite.frame.origin.y) + (cell.lblWebsite.frame.size.height);
        width   = [UIScreen mainScreen].bounds.size.width - x - 15;
        height  = myStringSize.height + 15;
        cell.lblDec.frame = CGRectMake(x, y, width, height);
        
    }else{
        cell.lblCompanyName.text = [NSString stringWithFormat:@"%@",compList.aryCompName[indexPath.row]];
        cell.lblWebsite.text = [NSString stringWithFormat:@"%@",compList.aryCompWebsite[indexPath.row]];
        cell.lblDec.text = [NSString stringWithFormat:@"%@",compList.aryCompDesc[indexPath.row]];
        
        NSString *url_Img_FULL = [NSString stringWithFormat:@"%@",compList.aryCompLogo[indexPath.row]];
        cell.imgLogo.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img_FULL]]];
        
        
        CGSize maximumSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 90, 9999);
        NSString *myString = [NSString stringWithFormat:@"%@",compList.aryCompDesc[indexPath.row]];
        CGSize myStringSize = [myString sizeWithFont:[UIFont systemFontOfSize:12.5]
                                   constrainedToSize:maximumSize
                                       lineBreakMode:cell.lblDec.lineBreakMode];
        
        CGFloat x = 0, y = 0, width = 0, height = 0;
        
        x   = (cell.lblCompanyName.frame.origin.x);
        y   = (cell.lblWebsite.frame.origin.y) + (cell.lblWebsite.frame.size.height);
        width   = [UIScreen mainScreen].bounds.size.width - x - 15;
        height  = myStringSize.height + 15;
        cell.lblDec.frame = CGRectMake(x, y, width, height);
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isFiltered == YES){
        
        UILabel *lblDec = [[UILabel alloc]init];
        lblDec.font = [UIFont systemFontOfSize:12.5];
        lblDec.textColor = [UIColor grayColor];
        lblDec.numberOfLines = 0;
        
        CGSize maximumSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 90, 9999);
        NSString *myString = [NSString stringWithFormat:@"%@",sortObject.aryCompDesc[indexPath.row]];
        CGSize myStringSize = [myString sizeWithFont:[UIFont systemFontOfSize:12.5]
                                   constrainedToSize:maximumSize
                                       lineBreakMode:lblDec.lineBreakMode];
        
        return 70 + myStringSize.height + 20;
        
    }else{
        UILabel *lblDec = [[UILabel alloc]init];
        lblDec.font = [UIFont systemFontOfSize:12.5];
        lblDec.textColor = [UIColor grayColor];
        lblDec.numberOfLines = 0;
        
        CGSize maximumSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 90, 9999);
        NSString *myString = [NSString stringWithFormat:@"%@",compList.aryCompDesc[indexPath.row]];
        CGSize myStringSize = [myString sizeWithFont:[UIFont systemFontOfSize:12.5]
                                   constrainedToSize:maximumSize
                                       lineBreakMode:lblDec.lineBreakMode];
        
        return 70 + myStringSize.height + 20;
    }
    
    
}*/

#pragma mark - SEARCH BAR DELEGATE

#pragma mark - SEARCH BAR DELEGATE

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text = @"";
    [searchBar resignFirstResponder];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}

#pragma mark - FILTER BY RESTAURANT

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
  /*  if (searchText.length != 0) {
       // strSearchString = searchText;
        
        NSLog(@"hddgdh:%@",searchText);
        
        NSString *searchTexts = searchText;
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchTexts]; // if you need case sensitive search avoid '[c]' in the predicate
        
        aryFilterResults = [compList.aryCompName filteredArrayUsingPredicate:predicate];
        
        NSLog(@"RESULT:%@",aryFilterResults);
        
        
       
    }else{
        [searchList endEditing:YES];
    }*/
    
    if (searchText.length != 0) {
        strSearchString = searchText;
        
        NSLog(@"Searchbar call");
    }else{
        strSearchString = @"";
        [searchList endEditing:YES];
    }
    if (!sortObject) {
        sortObject = [[SortingObject alloc]initWithObject:compList SearchString:strSearchString];
        sortObject.delegate = self;
        
    }else{
        [sortObject fetchObject:compList SearchString:strSearchString];
    }
}

#pragma mark - SEARCH EVENT
-(void)searchEvent{
    btnSearch.hidden  = YES;
    lblTitle.hidden   = YES;
    btnSort.hidden = NO;
    searchList.hidden = NO;
    
    NSLog(@"search event call when button pressed");
}

#pragma mark -FILTER EVENT

-(void)filterEvent{
    btnSearch.enabled   = YES;
    if (compList.aryCompName.count != 0) {
        [UIView animateWithDuration:1.0f
                              delay:0.0
             usingSpringWithDamping:1.0
              initialSpringVelocity:0.1
                            options:0 animations:^{
                                [self.view addSubview:viewParameter];
                                viewParameter.frame  = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 49);
                               
                              //  [scrollList removeFromSuperview];
                                [viewNavbar removeFromSuperview];
                            }
                         completion:nil];
    }
}
#pragma mark - FILTER BACK EVENT
-(void)filterBackEvent{
    [UIView animateWithDuration:1.0f
                          delay:0.0
         usingSpringWithDamping:1.0
          initialSpringVelocity:0.1
                        options:0 animations:^{
                         //   [self.view addSubview:scrollList];
                            [self.view addSubview:viewNavbar];
                            viewParameter.frame = CGRectMake(0,-[UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height - 49);
                        }
                     completion:nil];
    [self clearFilter];
}

-(void)clearFilter{
    
    strSearchString = @"";
    
    btnAscending.backgroundColor = [UIColor whiteColor];
    btnDescending.backgroundColor = [UIColor whiteColor];
    
    [btnAscending setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [btnDescending setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    if (!sortObject) {
        sortObject = [[SortingObject alloc]initWithObject:compList SearchString:strSearchString];
        sortObject.delegate = self;
    }else{
        [sortObject fetchObject:compList SearchString:strSearchString];
    }
    
    [self frameForView];
    
}

#pragma mark - Table Height

-(void)Tableheight{
    tableheight = 0;
    if (isFiltered){
        for (NSInteger i = 0; i < sortObject.aryCompname.count; ++i) {
            /*if ([sortObject.aryRestOffer[i] isEqualToString:@"Yes"]){
                tableheight = tableheight + 135;
            }else{
                tableheight = tableheight + 115;
            }*/
            tableheight = tableheight + 135;
        }
    }else{
        for (NSInteger j = 0; j < compList.aryCompName.count; ++j) {
            
            tableheight = tableheight + 115;
            /*if ([listRestaurant.aryRestOffer[j] isEqualToString:@"Yes"]){
                tableheight = tableheight + 135;
            }else{
                tableheight = tableheight + 115;
            }*/
        }
    }
}

#pragma mark - SORT OBJECT DELEGATE

-(void)SortingObject:(SortingObject *)sortingObject{
    isFiltered = sortObject.isFiltered;
   // [self Tableheight];
    [tblCompayView reloadData];
    [self frameForView];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

@end
